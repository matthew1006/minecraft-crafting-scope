var scopes = require('unity-js-scopes')
var http = require('http');
var XML = require('pixl-xml');

//var jsdom = require("jsdom");
//var window = jsdom.jsdom().defaultView;
//var $ = require('jquery')(window);

var query_host = "www.minecraftcraftingguide.net"
var search_path = "/search/?s="
var wiki_path = "http://minecraft.gamepedia.com/"
var wiki_search_path = "index.php?search="

var pattern = /<tr>\n\s*<td.+<strong.*>(.+)<\/s(.*\n){5}.*src='(.+)' a.*\n.*<span>(.+)<(.*\n){3}.*<td>(.+)</g;

var WIKI_TEMPLATE =
        {
    "schema-version": 1,
    "template": {
        "category-layout": "grid",
        "card-layout": "horizontal",
        "card-size": "large",
    },
    "components": {
        "title": "title",
        "subtitle": "subtitle"
    }
}

var RESULT_TEMPLATE =
        {
    "schema-version": 1,
    "template": {
        "category-layout": "grid",
        "card-layout": "vertical",
        "card-size": "medium",
    },
    "components": {
        "title": "title",
        "art": {
            "field": "art",
            "aspect-ratio": 1.8
        },
        "description": "description",
        "ingredients": "ingredients"
    }
}

var scopeId = -1;

scopes.self.initialize(
            {}
            ,
            {
                run: function() {
                    console.log('Running...')
                },
                start: function(scope_id) {
                    console.log('Starting scope id: '
                                + scope_id
                                + ', '
                                + scopes.self.scope_directory);
                    scopeId = scope_id;
                },
                search: function(canned_query, metadata) {
                    return new scopes.lib.SearchQuery(
                                canned_query,
                                metadata,
                                // run
                                function(search_reply) {
                                    var qs = canned_query.query_string();
                                    if (!qs) {
                                        qs = "Block"
                                    }

                                    console.log(canned_query.to_uri());
                                    var forecase_weather_cb = function(response) {
                                        var res = '';

                                        // Another chunk of data has been recieved, so append it to res
                                        response.on('data', function(chunk) {
                                            res += chunk;
                                        });

                                        // The whole response has been recieved
                                        response.on('end', function() {

                                            try {
                                                var category_renderer = new scopes.lib.CategoryRenderer(JSON.stringify(RESULT_TEMPLATE));
                                                var category = search_reply.register_category("Search Results", "", "", category_renderer);

                                                var match = pattern.exec (res);

                                                while (match !== null)
                                                {
                                                    var categorised_result = new scopes.lib.CategorisedResult(category);

                                                    categorised_result.set_title(match[1]);
                                                    categorised_result.set("art", match[3]);
                                                    categorised_result.set("description", match[4]);
                                                    categorised_result.set("ingredients", match[6]);
                                                    categorised_result.set_uri(wiki_path + encodeURIComponent(match[1]));

                                                    search_reply.push(categorised_result);

                                                    match = pattern.exec (res);
                                                }

                                                search_reply.finished();
                                            }
                                            catch(e) {
                                                // Forecast not available
                                                console.log("Forecast for '" + qs + "' is unavailable: " + e)

                                            }
                                        });
                                    }

                                    http.request({host: query_host, path: search_path + encodeURIComponent(qs)}, forecase_weather_cb).end();

                                    // We are done, call finished() on our search_reply
                                },
                                // cancelled
                                function() {
                                });
                },
                preview: function(result, action_metadata) {
                    return new scopes.lib.PreviewQuery(
                                result,
                                action_metadata,
                                // run
                                function(preview_reply) {

                                    var r = this.result();

                                    var layout1col = new scopes.lib.ColumnLayout(1);
                                    var layout2col = new scopes.lib.ColumnLayout(2);
                                    var layout3col = new scopes.lib.ColumnLayout(3);

                                    layout1col.add_column(["image", "header", "summary", "ingredients", "action"]);

                                    layout2col.add_column(["image"]);
                                    layout2col.add_column(["header", "summary", "ingredients", "action"]);

                                    layout3col.add_column(["image"]);
                                    layout3col.add_column(["header", "summary"]);
                                    layout3col.add_column(["ingredients", "action"]);

                                    preview_reply.register_layout([layout1col, layout2col, layout3col]);

                                    var header = new scopes.lib.PreviewWidget("header", "header");
                                    header.add_attribute_mapping("title", "title");

                                    var image = new scopes.lib.PreviewWidget("image", "image");
                                    image.add_attribute_mapping("source", "art");

                                    var description = new scopes.lib.PreviewWidget("summary", "text");
                                    description.add_attribute_mapping("text", "description");

                                    var ingredients = new scopes.lib.PreviewWidget("ingredients", "expandable");
                                    ingredients.add_attribute_value("title", "Ingredients");
                                    //ingredients.add_attribute_value("collapsed-widgets", 2);

                                    var ing = r.get("ingredients").split(" + ");
                                    var ing_actions = new Array();

                                    try
                                    {
                                        for (var i in ing)
                                        {
                                            ing_actions[i] = new scopes.lib.PreviewWidget(ing[i], "icon-actions");
                                            //list[i].add_attribute_value("text", ing[i]);

                                            var q = new scopes.lib.CannedQuery (scopeId, ing[i], "");

                                            ing_actions[i].add_attribute_value(
                                                            "actions",
                                                            {
                                                                "id": ing[i] + "_action",
                                                                "label": ing[i],
                                                                "uri": q.to_uri()
                                                            }
                                                        );

                                            ingredients.add_widget (ing_actions[i]);
                                        }
                                    }
                                    catch (e)
                                    {
                                        console.log(e.name);
                                        console.log(e.message);
                                        console.log(e.stack);
                                        ingredients.add_attribute_value("title", e.message);
                                    }

                                    var action = new scopes.lib.PreviewWidget("action", "actions");
                                    action.add_attribute_value(
                                                    "actions",
                                                    {
                                                        "id": "wiki",
                                                        "label": "Wiki",
                                                        "uri": r.get("uri")
                                                    }
                                                );

                                    preview_reply.push([image, header, description, ingredients, action].concat (ing_actions));
                                    preview_reply.finished();

                                },
                                // cancelled
                                function() {
                                });
                }
            }
            );

